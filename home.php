<html>
<title>HomePage</title>
<link href="home.css"rel="stylesheet"type="text/css"/>
<body>
<div class="main">
   <?php include("header.php"); ?>
   <?php include("mainmenu.php"); ?>
   <?php include("slider.php"); ?>
  <div class="homepage">
   <h2><marquee>Prime Medical College Hospital,Rangpur</marquee></h2>
 <p>
 Prime Medical College is a school of medicine in Rangpur, Bangladesh. It is established by Dr. Akkas Ali Sarkar in 2008.
 At present the college is providing Bangladesh government-directed medical education under the University of Rajshahi.
 Prime Medical College is trying to be one of the trendsetter institutes in the country and demonstrate a high standard of medical education.
 </p>
 <p>
 

    Prime Medical College Hospital is the teaching hospital of the college which started on 17 August 2007. It is a 500-bedded (750 beds proposed) hospital.
</p>
<p>
    The hospital is a seven-stored building with all the modern facilities. It is a specialized hospital in the northern region of Bangladesh which has 
	well-equipped discipline of medicine, General Surgery, Obstetrical, Paediatric, Anesthesiology, Cardiology, Gastroenterology, Nephrology, Neurology,
	Dermatology, Ophthalmology, Orthopedic Surgery, ENT, Urology, Neurosurgery unit and Neonatal ICU.
</p>
	<p>
    The Diagnostic Lab is well equipped with all the modern sophisticated medical equipment for Biochemistry, Haematology, Microbiology and Histopathology
	examination.
</p>
    
 <p>Prime Medical College and Hospital has got:</p>
 <ul>
   <li><p>Permission by the Ministry of Health of Family Welfare, Government of the People's Republic of Bangladesh in 2008</p></li>
   <li><p>Affiliation from the University of Rajshahi</p></li>
   <li><p>Recognition of training by Bangladesh Medical and Dental Council (BMDC).</p></li>
</ul>
</div>
<div class="footer">
   <p>&copy;Prime medical College Hospital All Reserved</p>
   </div>
</div>
</body>
</html>